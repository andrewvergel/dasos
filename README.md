# Configuracion de Hyperledger con tu propio dominio

Configurar Hyperledger (one peer)

---
## Summary

- [Prerequisites](#prerequisites)
- [Installing components](#installing-components)
---

### Prerequisites

To run `bashHyperledger Composer` and `Hyperledger Fabric`, I recommend you have at least 4Gb of memory and 20 GB of HdDisk

The following are prerequisites for installing the required development tools:

- Operating Systems: Ubuntu Linux 16.04 64-bit.
- Docker Engine: Version 17.12.1 or higher
- Docker-Compose: Version 1.13 o
- Node: 8.10.0 or higher (version 9 is not supported)
- npm: v5.7.1
- git: 2.9.x or higher
- Python: 2.7.12


Before to install  the prerequisites, we have to download the project from bitbucker
```bash
cd ~ && git clone https://rolivasilva@bitbucket.org/rolivasilva/dasos.git
```

If you have an installation is clean, you must install the prerequisites
```bash
cd ~/hyperledger/config && ./prereqsUbuntu.sh
```

**Please, remember to reload the enviroment variable**
---

### Installing components

There are a few useful CLI tools to used. The most important one is `composer-cli`, which contains all the essential operations, also pick up `generator-hyperledger-composer`, `composer-rest-server` and `Yeoman` plus the generator-hyperledger-composer.

- Installing the Utilities: Essential CLI tools, REST Server, generating application assets, Yeoman and "Playground" UI  :
```bash
npm install -g composer-cli
npm install -g composer-rest-server
npm install -g generator-hyperledger-composer
npm install -g yo
npm install -g composer-playground
```

**Move the folder to the user's home, if you prefer not to move it, remember to change the route of the scripts that are run in the next steps**
---

### Initializing fabric

Initializing fabric and deploy the containers

```bash
cd ~/hyperledger
./startFabric.sh
./createPeerAdminCard.sh
```

### Start the web app ("Playground") and background

only is some way of run in background, this run by port 8080
```bash
composer-playground
```

Generate a business network archive
```bash
cd it-networks
composer archive create -t dir -n .
```
Deploying the business network
```bash
composer runtime install --card PeerAdmin@hlfv1 --businessNetworkName dasos-network
```
The composer network start command requires a business network card

```bash
composer network start --card PeerAdmin@hlfv1 --networkAdmin admin --networkAdminEnrollSecret adminpw --archiveFile dasos-network@0.0.1.bna --file networkadmin.card
```

To import the network administrator identity as a usable business network card

```bash
composer card import --file networkadmin.card
```

*To check that the business network has been deployed successfully, run the following command to ping the network*

```bash
composer network ping --card admin@dasos-network
```

Generating a REST server, port 3000

```bash
composer-rest-server --card  admin@dasos-network --namespaces never
```
