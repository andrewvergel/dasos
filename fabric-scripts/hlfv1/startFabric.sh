#!/bin/bash

# Exit on first error, print all commands.
set -ev

#Detect architecture
ARCH=`uname -m`

# Grab the current directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#

ARCH=$ARCH docker-compose -f "${DIR}"/composer/docker-compose.yml down
ARCH=$ARCH docker-compose -f "${DIR}"/composer/docker-compose.yml up -d

# wait for Hyperledger Fabric to start
# incase of errors when running later commands, issue export FABRIC_START_TIMEOUT=<larger number>
echo ${FABRIC_START_TIMEOUT}
sleep ${FABRIC_START_TIMEOUT}

# Create the channel
docker exec peer0.org1.dasos.io peer channel create -o orderer.dasos.io:7050 -c composerchannel -f /etc/hyperledger/configtx/composer-channel.tx

# Join peer0.org1.dasos.io to the channel.
docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.dasos.io/msp" peer0.org1.dasos.io peer channel join -b composerchannel.block
./createPeerAdminCard.sh
#cd dasos-network
#nohup composer-playground > playground.out 2> playground.err < /dev/null &
#composer runtime install --card PeerAdmin@hlfv1 --businessNetworkName dasos-network
#composer network start --card PeerAdmin@hlfv1 --networkAdmin admin --networkAdminEnrollSecret adminpw --archiveFile dasos-network@0.0.1.bna --file networkadmin.card
#composer card import --file networkadmin.card
#nohup composer-rest-server --card  admin@dasos-network --namespaces never > rest.out 2> rest.err < /dev/null &

